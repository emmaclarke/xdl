from xdl.constants import REAGENT_PROP_TYPE
from xdl.steps.templates.abstract_template import AbstractXDLElementTemplate
from xdl.utils.prop_limits import (
    CONCENTRATION_PROP_LIMIT,
    DENSITY_PROP_LIMIT,
    MOLECULAR_FORMULA_PROP_LIMIT,
    MOLECULAR_WEIGHT_PROP_LIMIT,
    PERCENT_RANGE_PROP_LIMIT,
    TEMP_PROP_LIMIT,
)


class AbstractReagent(AbstractXDLElementTemplate):
    """Reagent used by procedure.

    Name: Reagent

    Mandatory props:
        name (str): Name of reagent
        inchi (str): INCHI string of reagent
        cas (str): CAS number of reagent
        role (str): Role of reagent. One of ``'reagent'``, ``'substrate'``,
            ``'catalyst'``, ``'acid'``, ``'base'``, ``'solvent'``, ``'ligand'``,
            ``'quenching-agent'`` or ``'activating-agent'``.
        preserve (bool): If ``True``, reagent is expensive and should be used
            sparingly.
        use_for_cleaning (bool): If ``True``, the reagent is cheap and can be
            used for cleaning.
        clean_with (reagent): Name of another reagent that should be used when
            cleaning vessels that have come into contact with this reagent.
        stir (bool): Stir reagent flask for the entire procedure.
        temp (float): Cool (or heat) reagent flask to given temperature for the
            entire procedure.
        atmosphere (str): Store reagent under given gas for entire procedure.
        purity (float): Purity of reagent in %.
    """

    MANDATORY_NAME = "Reagent"

    MANDATORY_PROP_TYPES = {
        "name": str,
        "inchi": str,
        "cas": int,
        "molecular_formula": str,
        "molecular_weight": float,
        "density": float,
        "concentration": float,
        "role": str,
        "preserve": bool,
        "use_for_cleaning": bool,
        "clean_with": REAGENT_PROP_TYPE,
        "stir": bool,
        "temp": float,
        "atmosphere": str,
        "purity": float,
    }

    MANDATORY_DEFAULT_PROPS = {
        "inchi": None,
        "cas": None,
        "molecular_formula": None,
        "molecular_weight": None,
        "density": None,
        "concentration": None,
        "role": "reagent",
        "preserve": False,
        "use_for_cleaning": False,
        "clean_with": None,
        "stir": False,
        "temp": None,
        "atmosphere": None,
        "purity": None,
    }

    MANDATORY_PROP_LIMITS = {
        "molecular_formula": MOLECULAR_FORMULA_PROP_LIMIT,
        "molecular_weight": MOLECULAR_WEIGHT_PROP_LIMIT,
        "density": DENSITY_PROP_LIMIT,
        "concentration": CONCENTRATION_PROP_LIMIT,
        "temp": TEMP_PROP_LIMIT,
        "purity": PERCENT_RANGE_PROP_LIMIT,
    }
